const opn = require( "opn" );

opn( "test/index.html" )
	.catch( function() {
		console.error( "Unable to find test files." );
	} );
