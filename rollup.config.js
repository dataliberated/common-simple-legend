import { terser } from "rollup-plugin-terser";

export default {
	input: "./src/js/main.js",
	output: {
		name: "common.util.simplelegend",
		file: "./dist/common-simple-legend.min.js",
		format: "iife",
		sourceMap: "inline",
		globals: {
			"jQuery": "jQuery",
			"d3": "d3"
		}
	},
	external: [
		"d3"
	],
	plugins: [
		terser()
	]
};
