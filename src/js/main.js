import * as d3 from "d3";

/* global jQuery */

/* CONFIG OPTIONS
 * ***************************************************
 * data - Dataset for legend to represent.
 * fontSize - Font size for legend text.
 * entryGapSize - Gap between legend entries.
 * markerGapSize - Gap between markers and text.
 * markerSize - Size of markers.
 * labelHandler - Function that returns each entry's tabel.
 * colourHandler - Function that returns each entry's fill colour.
 * lineSpacing - Spacing between rows of legend entries. */

if ( jQuery !== null ) {
	jQuery.fn.SimpleLegend = function( config ) {
		var simpleLegendInstance = new SimpleLegend( this[ 0 ], config );

		return simpleLegendInstance;
	};
}

export function SimpleLegend( target, config ) {

	var data = config.data;
	var fontSize = config.fontSize !== undefined ? config.fontSize : 8;
	var fontColor = config.fontColor !== undefined ? config.fontColor : "#000000";
	var markerSize = config.markerSize !== undefined ? config.markerSize : 10;
	var markerGapSize = config.markerGapSize !== undefined ? config.markerGapSize : 2;
	var entryGapSize = config.entryGapSize !== undefined ? config.entryGapSize : 10;
	var lineSpacing = config.lineSpacing !== undefined ? config.lineSpacing : 5;
	var labelHandler = config.labelHandler !== undefined ? config.labelHandler :
		function() {
			return "";
		};
	var colourHandler = config.colourHandler !== undefined ? config.colourHandler :
		function() {
			return "#000000";
		};
	var mouseoverHandler = config.mouseoverHandler !== undefined ? config.mouseoverHandler :
		function() {
		};
	var mouseoutHandler = config.mouseoutHandler !== undefined ? config.mouseoutHandler :
		function() {
		};

	var svg = d3.select( target ).append( "svg" )
		.style( "width", "100%" );
		//.attr( "height", "" + 0 + "px" );

	var svgDimensions = svg.node().getBoundingClientRect();

	var yPos = 0;
	var xPos = 0;

	var absoluteRequiredSizeIncrease = markerSize;
	for ( var i = 0; i < data.length; i++ ) {

		var legendEntry = svg.append( "g" )
			.attr( "transform", "translate(" + xPos + "," + yPos + ")" );

		var rectColour = colourHandler( data[ i ], i, data );

		var legendMarker = legendEntry.append( "rect" )
			.attr( "fill", rectColour )
			.attr( "width", markerSize )
			.attr( "height", markerSize )
			.attr( "rx", markerSize / 10 )
			.attr( "ry", markerSize / 10 )
			.attr( "id", function() {
				return "entry-" + i;
			} )
			.on( "mouseover", mouseoverHandler )
			.on( "mouseout", mouseoutHandler );

		var legendMarkerDimensions = legendMarker.node().getBBox();

		var legendText = legendEntry.append( "text" )
			.attr( "x", markerGapSize + markerSize )
			.attr( "font-size", fontSize )
			.attr( "fill", fontColor )
			.attr( "dominant-baseline", "middle" )
			.text( labelHandler( data[ i ], i, data ) );

		legendText.attr(
			"y",
			legendMarkerDimensions.height / 2
		);

		var legendDimensions = legendEntry.node().getBBox();
		if (
			xPos + legendDimensions.width >= svgDimensions.width &&
			xPos !== 0
		) {
			absoluteRequiredSizeIncrease += lineSpacing + markerSize;
			xPos = 0;
			yPos += legendMarkerDimensions.height + lineSpacing;
			legendEntry.attr( "transform", "translate(" + xPos + "," + yPos + ")" );
		}
		xPos += legendDimensions.width + entryGapSize;

	}

	var svgDimensions = svg.node().getBoundingClientRect();
	svg.attr( "viewBox", "0 0 " + svgDimensions.width + " " + absoluteRequiredSizeIncrease );

}

